import axios from 'axios';

export const get = (url, options) => {
  return axios
    .get(process.env.REACT_APP_API_URL + url, options)
    .then(res => res.data)
    .catch(error => {
      throw new Error(error.response.statusText);
    });
};

export const update = (url, data, options) => {
  return axios
    .patch(process.env.REACT_APP_API_URL + url, data, {
      ...options,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.data)
    .catch(error => {
      throw new Error(error.response.statusText);
    });
};

export default {
  get,
  update
};
