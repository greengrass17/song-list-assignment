import isEmpty from 'lodash.isempty';

export default (options = {}) => {
  return Object.keys(options).reduce((result, key) => {
    if (isEmpty(options[key])) {
      return result;
    }
    return result += `&${key}=${options[key]}`
  }, '');
}