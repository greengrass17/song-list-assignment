import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  withStyles,
  Select,
  MenuItem,
  InputLabel,
  Input,
  FormControl
} from '@material-ui/core';
import isFunction from 'lodash.isfunction';

const styles = {};

export class LevelSelect extends Component {
  static propTypes = {
    onChange: PropTypes.func
  };

  state = {
    value: ''
  };

  onChange = event => {
    this.setState({ value: event.target.value });
    if (this.props.onChange && isFunction(this.props.onChange)) {
      this.props.onChange(event);
    }
  };

  render() {
    const { classes } = this.props;

    return (
      <FormControl className={classes.root}>
        <InputLabel shrink htmlFor="level-select">
          Level
        </InputLabel>
        <Select
          name="level"
          value={this.state.value}
          onChange={this.onChange}
          input={<Input name="level" id="level-select" />}
        >
          <MenuItem value="">None</MenuItem>
          {Array.from(Array(15), (_, index) => index + 1).map(value => (
            <MenuItem key={value} value={value}>
              {value}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  }
}

export default withStyles(styles)(LevelSelect);
