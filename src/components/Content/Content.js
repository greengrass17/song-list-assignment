import React, { Component } from 'react'
import { withStyles } from "@material-ui/core";
import { grey } from '@material-ui/core/colors';

const styles = theme => ({
  root: {
    padding: 0,
    height: '100vh',
    boxSizing: 'border-box',
    paddingTop: 64,
    overflowY: 'scroll',
    backgroundColor: grey[900],
    [theme.breakpoints.up('sm')]: {
      padding: 25,
    }
  }
})

export class Content extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {this.props.children}
      </div>
    )
  }
}

export default withStyles(styles)(Content)