import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SongListItem from './components/SongListItem';
import {
  withStyles,
  Paper,
  CircularProgress,
  Typography
} from '@material-ui/core';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchSongs } from '../../states/songs/songs.actions';
import { Waypoint } from 'react-waypoint';
import isEqual from 'lodash.isequal';

const styles = theme => ({
  root: {
    maxWidth: 700,
    margin: '0 auto',
    [theme.breakpoints.up('sm')]: {
      marginTop: 60
    }
  },
  loader: {
    textAlign: 'center',
    padding: 5
  },
  empty: {
    padding: 15,
    textAlign: 'center',
    fontSize: '1em'
  }
});

export class SongList extends Component {
  static propTypes = {
    songs: PropTypes.object,
    config: PropTypes.object
  };

  state = {}

  getMoreSongs = () => {
    const { songs, config } = this.props;
    if (!songs.allLoaded) {
      this.props.fetchSongs({
        ...config,
        page: songs.page + 1
      });
    }
  };

  componentWillReceiveProps(nextProps) {
    if (!isEqual(nextProps.config, this.props.config)) {
      this.props.fetchSongs(nextProps.config);
    }
  }

  componentDidMount() {
    this.props.fetchSongs(this.props.config);
  }

  render() {
    const { songs, classes } = this.props;
    return (
      <Paper className={classes.root}>
        {songs.items.map((song, index) => (
          <SongListItem key={index} song={song} />
        ))}
        {songs.loading && (
          <div className={classes.loader}>
            <CircularProgress size={20} />
          </div>
        )}
        {!songs.loading && songs.allLoaded && !songs.items.length && (
          <Typography className={classes.empty}>No songs found</Typography>
        )}
        {!songs.loading && !songs.allLoaded && <Waypoint onEnter={this.getMoreSongs} />}
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  songs: state.songsState
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchSongs }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(SongList));
