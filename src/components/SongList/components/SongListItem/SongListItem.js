import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Divider,
  withStyles,
  CircularProgress,
  Typography,
  IconButton
} from '@material-ui/core';
import Rating from 'react-rating';
import Icon from '@material-ui/core/Icon';
import Logo from './img/fingerprint-white.png';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateSong } from '../../../../states/songs/songs.actions';

const styles = theme => ({
  root: {
    textAlign: 'left',
    display: 'flex',
    position: 'relative',
    padding: 15,
    [theme.breakpoints.up('sm')]: {
      position: 'static',
      padding: 10
    }
  },
  section1: {
    position: 'absolute',
    opacity: 0.25,
    width: 120,
    height: '100%',
    left: 0,
    top: 0,
    paddingLeft: 0,
    [theme.breakpoints.up('sm')]: {
      flex: '0 0 120px',
      boxSizing: 'border-box',
      paddingLeft: 40,
      position: 'static',
      opacity: 1,
      width: 'auto',
      height: 'auto'
    }
  },
  section2: {
    flex: '0 0 auto',
    padding: 10,
    zIndex: 1,
    [theme.breakpoints.up('sm')]: {
      flex: '0 0 80px',
      boxSizing: 'border-box',
      padding: 'auto',
      zIndex: 'auto'
    }
  },
  section3: {
    zIndex: 1,
    boxSizing: 'border-box',
    padding: '5px 0',
    flex: '1 1 auto',
    [theme.breakpoints.up('sm')]: {
      zIndex: 'auto'
    }
  },
  thumbnail: {
    borderRadius: 0,
    boxShadow: '-30px 0px 60px 4px #000 inset',
    backgroundColor: theme.palette.primary.main,
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      boxShadow: 'none',
      borderRadius: 10
    }
  },
  thumbnailLogo: {
    width: 40
  },
  level: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    position: 'relative'
  },
  levelText: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%,-50%)'
  },
  title: {
    maxWidth: 330,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden'
  },
  footer: {
    display: 'flex',
    alignItems: 'center',
    marginTop: 5
  },
  rating: {
    marginRight: 10
  },
  artist: {
    marginRight: 10,
    maxWidth: 150,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden'
  },
  starIcon: {
    fontSize: 12,
    width: '1.2em'
  },
  placeholder: {
    backgroundColor: theme.palette.action.disabledBackground
  },
  placeholderLevel: {
    width: 10,
    height: 15,
    padding: '2.5px 0'
  },
  placeholderTitle: {
    width: 330,
    height: 15,
    padding: '2.5px 0'
  },
  placeholderArtist: {
    width: 150,
    height: 15,
    padding: '2.5px 0'
  },
  placeholderViews: {
    width: 35,
    height: 15,
    padding: '2.5px 0'
  }
});

export class SongListItem extends Component {
  static propTypes = {
    song: PropTypes.object
  };

  static defaultProps = {
    song: {}
  };

  updateRating = rating => {
    const { song } = this.props;
    this.props.updateSong(song.id, { rating });
  };

  renderPlaceholder(className) {
    const { classes } = this.props;
    return (
      <div className={classNames([classes.placeholder, classes[className]])} />
    );
  }

  render() {
    const { classes, song } = this.props;
    return (
      <React.Fragment>
        <div className={classes.root}>
          <div className={classes.section1}>
            <div className={classes.thumbnail}>
              <img
                src={Logo}
                alt="placeholder-thumbnail"
                className={classes.thumbnailLogo}
              />
            </div>
          </div>
          <div className={classes.section2}>
            <div className={classes.level}>
              <CircularProgress
                className={classes.levelCircle}
                value={(song.level * 100) / 15}
                variant="static"
                size={30}
              />
              <Typography className={classes.levelText} color="default">
                {song.level || this.renderPlaceholder('placeholderLevel')}
              </Typography>
            </div>
          </div>
          <div className={classes.section3}>
            <Typography className={classes.title}>
              {song.title || this.renderPlaceholder('placeholderTitle')}
            </Typography>
            <div className={classes.footer}>
              <Typography className={classes.rating}>
                <Rating
                  fractions={10}
                  initialRating={song.rating}
                  onChange={this.updateRating}
                  emptySymbol={
                    <Icon
                      fontSize="small"
                      className={classNames(classes.starIcon, 'fa fa-star')}
                      color="disabled"
                      component="i"
                    />
                  }
                  fullSymbol={
                    <Icon
                      fontSize="small"
                      className={classNames(classes.starIcon, 'fa fa-star')}
                      color="secondary"
                      component="i"
                    />
                  }
                />
              </Typography>
              <Typography className={classes.artist} color="textSecondary">
                {song.artist || this.renderPlaceholder('placeholderArtist')}
              </Typography>
              <Icon fontSize="small" color="disabled">
                play_arrow
              </Icon>
              <Typography className={classes.views} color="textSecondary">
                {song.views || this.renderPlaceholder('placeholderViews')}
              </Typography>
            </div>
          </div>
          <div className={classes.section4}>
            <IconButton>
              <Icon>more_horiz</Icon>
            </IconButton>
          </div>
        </div>
        <Divider />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateSong }, dispatch);
};

export const StyledSongListItem = withStyles(styles)(SongListItem);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(SongListItem));
