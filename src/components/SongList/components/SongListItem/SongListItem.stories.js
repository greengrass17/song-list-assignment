import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from "@storybook/addon-actions";
import { StyledSongListItem } from './SongListItem';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { theme } from '../../../..';
import Paper from '@material-ui/core/Paper';

const style = {
  width: 700
}

storiesOf('SongListItem', module)
  .add('empty', () => (
    <MuiThemeProvider theme={theme}>
      <Paper style={style}>
        <StyledSongListItem {...getProps()} />
      </Paper>
    </MuiThemeProvider>
  ))
  .add('long title', () => (
    <MuiThemeProvider theme={theme}>
      <Paper style={style}>
        <StyledSongListItem
          {...getProps({
            song: {
              id: 1,
              artist: 'The Yousicians',
              title: 'This is a very very very very very very very very very very very very very very very very very very very very very very very very very very very long title',
              level: 13,
              released: '2016-10-26',
              views: '1.5M',
              rating: 5
            }
          })}
        />
      </Paper>
    </MuiThemeProvider>
  ))
  .add('long artist', () => (
    <MuiThemeProvider theme={theme}>
      <Paper style={style}>
        <StyledSongListItem
          {...getProps({
            song: {
              id: 1,
              title: 'The Yousicians',
              artist: 'This is a very very very very very very very very very very very very very very very very very very very very very very very very very very very long artist',
              level: 13,
              released: '2016-10-26',
              views: '1.5M',
              rating: 5
            }
          })}
        />
      </Paper>
    </MuiThemeProvider>
  ));

const getProps = props => ({
  classes: {},
  updateSong: action('update'),
  ...props
});
