import songsTypes from './songs.types';

export const initialState = {
  items: [],
  page: -1,
  allLoaded: false,
  loading: false,
  error: null
};

export default (state = initialState, { type, payload = { items: [] } }) => {
  const itemsId = state.items.map(item => item.id);
  const page = payload.page || 0;
  switch (type) {
    case songsTypes.FETCH_SONGS:
      return {
        ...state,
        loading: true,
        items: payload === 0 ? [] : state.items
      };

    case songsTypes.FETCH_SONGS_SUCCESS:
      const allLoaded = payload.items.length === 0;
      const newItems = payload.items.filter(
        item => itemsId.indexOf(item.id) === -1
      );
      const mergedItems = allLoaded
        ? state.items
        : [...state.items, ...newItems];
      return {
        ...state,
        items: mergedItems,
        page: allLoaded ? state.page : page,
        loading: false,
        allLoaded
      };

    case songsTypes.FETCH_SONGS_FAILURE:
      return { ...state, loading: false, error: payload };

    case songsTypes.UPDATE_SONG_SUCCESS:
      const index = state.items.findIndex(item => item.id === payload.id)
      const clonedItems = [...state.items];
      clonedItems.splice(index, 1, payload.song);
      return { ...state, items: clonedItems }

    default:
      return state;
  }
};
