import songsTypes from './songs.types';
import ajax from '../../services/ajax';
import stringifyParams from '../../services/utils/stringifyParams';

const LIMIT = 10;

export const fetchSongs = (config = { page: 0 }) => dispatch => {
  const page = config.page || 0;
  dispatch({
    type: songsTypes.FETCH_SONGS,
    payload: page
  });
  return ajax
    .get(
      `/songs?_page=${page}&_limit=${LIMIT}${stringifyParams(config.options)}`
    )
    .then(songs => {
      dispatch({
        type: songsTypes.FETCH_SONGS_SUCCESS,
        payload: {
          items: songs,
          page
        }
      });
      return songs;
    })
    .catch(error => {
      dispatch({
        type: songsTypes.FETCH_SONGS_FAILURE,
        payload: error
      });
      return error;
    });
};

export const updateSong = (id, data) => dispatch => {
  dispatch({
    type: songsTypes.UPDATE_SONG
  });
  return ajax
    .update(`/songs/${id}`, data)
    .then(song => {
      dispatch({
        type: songsTypes.UPDATE_SONG_SUCCESS,
        payload: { id, song }
      });
    })
    .catch(error => {
      dispatch({
        type: songsTypes.UPDATE_SONG_FAILURE,
        payload: error
      });
      return error;
    });
};
