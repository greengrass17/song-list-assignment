import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import mockData from '../_mocks_/mockData';
import songsTypes from '../songs.types';
import { fetchSongs } from '../songs.actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('fetch songs actions', () => {
  beforeEach(() => moxios.install());
  afterEach(() => moxios.uninstall());

  it('dispatch FETCH_SONGS_SUCCESS when fetching songs success', async done => {
    const { songsResponse } = mockData;
    moxios.stubRequest('http://localhost:3004/songs?_page=0&_limit=10', {
      status: 201,
      response: songsResponse
    });
    const expectedActions = [
      { type: songsTypes.FETCH_SONGS, payload: 0 },
      {
        type: songsTypes.FETCH_SONGS_SUCCESS,
        payload: {
          items: songsResponse,
          page: 0
        }
      }
    ];
    const store = mockStore({});
    await store.dispatch(fetchSongs()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
    done();
  });

  it('make correct fetch songs request with config', async done => {
    const { songsResponse } = mockData;
    moxios.stubRequest('http://localhost:3004/songs?_page=1&_limit=10&title_like=Op&level=9', {
      status: 201,
      response: songsResponse
    });
    const expectedActions = [
      { type: songsTypes.FETCH_SONGS, payload: 1 },
      {
        type: songsTypes.FETCH_SONGS_SUCCESS,
        payload: {
          items: songsResponse,
          page: 1
        }
      }
    ];
    const store = mockStore({});
    await store.dispatch(fetchSongs({
      page: 1,
      options: {
        title_like: 'Op',
        level: '9'
      }
    })).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
    done();
  });

  it('dispatch FETCH_SONGS_FAILURE when fetching songs fails', async done => {
    const { songsError } = mockData;
    moxios.stubRequest('http://localhost:3004/songs?_page=0&_limit=10', {
      ...songsError
    });
    const expectedActions = [
      { type: songsTypes.FETCH_SONGS, payload: 0 },
      {
        type: songsTypes.FETCH_SONGS_FAILURE,
        payload: new Error(songsError.statusText)
      }
    ];
    const store = mockStore({});
    await store.dispatch(fetchSongs()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
    done();
  });

});
