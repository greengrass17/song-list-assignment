import songsReducers, { initialState } from '../songs.reducers';
import songsTypes from '../songs.types';

it('Set loading status correctly', () => {
  expect(
    songsReducers(getState(), {
      type: songsTypes.FETCH_SONGS
    })
  ).toMatchObject({
    loading: true
  });
});

it('Returns correctly for empty data', () => {
  expect(
    songsReducers(getState(), {
      type: songsTypes.FETCH_SONGS_SUCCESS
    })
  ).toMatchObject({
    items: [],
    page: -1
  });
});

it('Returns correctly for normal data', () => {
  expect(
    songsReducers(getState(), {
      type: songsTypes.FETCH_SONGS_SUCCESS,
      payload: {
        items: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }]
      }
    })
  ).toMatchObject({
    page: 0
  });
});

it('Correctly add new data', () => {
  expect(
    songsReducers(
      getState({
        items: [{ id: 1 }, { id: 2 }],
        page: 0
      }),
      {
        type: songsTypes.FETCH_SONGS_SUCCESS,
        payload: {
          items: [{ id: 3 }, { id: 4 }],
          page: 1
        }
      }
    )
  ).toMatchObject({
    page: 1,
    items: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }]
  });
});

it('Correctly skip duplicate data', () => {
  expect(
    songsReducers(
      getState({
        items: [{ id: 1 }, { id: 2 }]
      }),
      {
        type: songsTypes.FETCH_SONGS_SUCCESS,
        payload: {
          items: [{ id: 2 }, { id: 4 }],
          page: 1
        }
      }
    )
  ).toMatchObject({
    page: 1,
    items: [{ id: 1 }, { id: 2 }, { id: 4 }]
  });
});

it('Correctly signal all data loaded', () => {
  expect(
    songsReducers(
      getState({
        items: [{ id: 1 }, { id: 2 }],
      }),
      {
        type: songsTypes.FETCH_SONGS_SUCCESS,
        payload: {
          items: []
        }
      }
    )
  ).toMatchObject({
    allLoaded: true
  });
});

const getState = state => ({
  ...initialState,
  ...state
});
