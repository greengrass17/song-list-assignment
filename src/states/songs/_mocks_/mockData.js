export default {
  songsResponse: [{ id: 1 }, { id: 2 }, { id: 3 }],
  songsError: { status: 404, statusText: 'Songs not found' }
};
