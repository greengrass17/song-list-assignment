import songsReducers from "./songs/songs.reducers";
import { combineReducers } from "redux";

const reducerObj = {
  songsState: songsReducers
}

export default combineReducers(reducerObj)