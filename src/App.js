import React, { Component } from 'react';
import { Provider } from 'react-redux';
import './App.css';
import { withStyles } from '@material-ui/core';
import HomeView from './views/Home';

const styles = theme => ({
  root: {
    height: '100%',
    width: '100%',
    position: 'fixed',
    boxSizing: 'border-box'
  },
});

class App extends Component {
  render() {
    const { classes, store } = this.props;
    return (
      <Provider store={store}>
        <div className={classes.root}>
          <HomeView />
        </div>
      </Provider>
    );
  }
}

export default withStyles(styles)(App);
