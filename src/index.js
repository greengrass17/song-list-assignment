import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import AllReducers from './states';

export const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#4FC318'
    },
    secondary: {
      main: '#C1D3C0'
    },
    background: {
      paper: '#0C0C14'
    }
  },
  typography: {
    fontFamily: ['Montserrat', 'sans-serif'].join(',')
  }
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const configureStore = () =>
  createStore(AllReducers, composeEnhancers(applyMiddleware(thunk)));
export const store = configureStore();

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <App store={store} />
  </MuiThemeProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
