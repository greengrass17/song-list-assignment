import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';
import Content from '../../components/Content';
import SongList from '../../components/SongList';
import { bindActionCreators } from 'redux';
import { fetchSongs } from '../../states/songs/songs.actions';
import LevelSelect from '../../components/LevelSelect';
import SearchBar from '../../components/SearchBar';
import Logo from './img/fingerprint-white.png';

const styles = theme => ({
  grow: {
    flexGrow: 1
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  logo: {
    width: 30,
    marginRight: 15,
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    }
  }
});

export class HomeView extends Component {
  state = {
    config: { page: 0 }
  };

  handleChange = key => event => {
    const { config } = this.state;
    this.setState({
      config: {
        ...config,
        options: {
          ...config.options,
          [key]: '' + event.target.value
        }
      }
    });
  };

  componentDidMount() {
    document.title = 'Song List | Home'
  }

  render() {
    const { classes } = this.props;
    const { config } = this.state;
    return (
      <React.Fragment>
        <AppBar color="primary">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.title}>
              Song List
            </Typography>
            <img className={classes.logo} src={Logo} alt="logo" />
            <div className={classes.grow} />
            <SearchBar onChange={this.handleChange('title_like')} />
            <LevelSelect onChange={this.handleChange('level')} />
          </Toolbar>
        </AppBar>
        <Content>
          <SongList config={config} />
        </Content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchSongs }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(HomeView));
