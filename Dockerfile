FROM node:10.6.0-alpine

WORKDIR /usr/src/app

RUN rm -rfv /usr/src/app/*

COPY package.json ./
COPY yarn.lock ./

RUN yarn

COPY . .

RUN yarn build && \
    yarn cache clean

CMD yarn server

EXPOSE 8080