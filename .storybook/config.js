import { configure } from '@storybook/react';

function loadStories() {
  require('../src/components/SongList/components/SongListItem/SongListItem.stories')
}

configure(loadStories, module);
