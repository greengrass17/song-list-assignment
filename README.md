This is a web app to find songs for practice. Some features included:

- You can search for a song, or filter them by your level.
- You can rate the songs you favor.
- The songs can be loaded gradually to keep up performance.
- It works on different screen sizes.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Deploy and serve

You can use `Docker` to make deploying and serving easier. With `Docker` running, you can run the following command:

`docker-compose up --build`

Open [http://0.0.0.0:8080](http://0.0.0.0:8080) to view it in the browser.

## Available Scripts

To run the scripts below, it is required that you have `yarn` installed.<br>
In the project directory, you can run:

### `yarn`

Install all dependencies.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn server`

Runs the app in production mode.<br>
Open [http://0.0.0.0:8080](http://0.0.0.0:8080) to view it in the browser.

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn storybook`

Storybook is used as a testing tool for UI React Component.